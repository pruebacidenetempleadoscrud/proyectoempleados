# *REQUISITOS PREVIOS*:
1. MongoDB server
2. Tener instalado Java  v1.8

#### *INSTRUCTIVO DE EJECUCIÓN* 

0. Inicializar servidor base de datos MongoDB
1. Clonar el repositorio ProyectoEmpleados (Backend)
2. Ir a la carpeta Target dentro del proyecto y abrir cmd
3. Ejecutar comando `java -Dserver.port=8080 -jar empleados-0.0.1-SNAPSHOT.jar` (escoger un puerto libre)
4. Clonar el reposirorio ProyectoEmpleadosCliente (FrontEnd)
5. Abrir nueva terminal dentro del proyecto y ejecutar el siguiente comando `ng serve --open`
6. En caso de presentar error ng: `<rutadelproyecto>` cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170., ejecute el siguiente comando `Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process`
7. Vuelva a ejecutar el comando "ng serve --open"
8. Se desplegara una vista web en el explorador por defecto lista para usar o ingresa al navegador de preferencia  y entra a la siguiente url http://localhost:4200