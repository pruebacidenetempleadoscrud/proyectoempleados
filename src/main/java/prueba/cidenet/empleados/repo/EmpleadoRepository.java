package prueba.cidenet.empleados.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import prueba.cidenet.empleados.model.Empleado;

public interface EmpleadoRepository extends MongoRepository<Empleado, String>{
    
}
