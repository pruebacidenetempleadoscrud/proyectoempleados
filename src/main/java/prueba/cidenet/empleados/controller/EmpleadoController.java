package prueba.cidenet.empleados.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import prueba.cidenet.empleados.model.Empleado;
import prueba.cidenet.empleados.services.CrudServices;

import java.util.List;


@CrossOrigin
@RestController
@RequestMapping("/empleados")
public class EmpleadoController {
    
    @Autowired
    private CrudServices crudServices;

    @GetMapping("")
    List<Empleado> index(){
        return crudServices.consultarEmppleado();

    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    Empleado create(@RequestBody Empleado empleado) throws Exception{

        return crudServices.crearEmpleado(empleado);

    }

    @PutMapping("{id}")
    Empleado update(@PathVariable String id, @RequestBody Empleado empleado) throws Exception{

        
         return crudServices.editarEmpleado(id,empleado); 

    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{id}")
    void delete(@PathVariable String id){

        crudServices.eliminarEmpleado(id);
    }


}
