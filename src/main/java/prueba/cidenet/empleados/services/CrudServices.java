package prueba.cidenet.empleados.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import prueba.cidenet.empleados.model.Empleado;
import prueba.cidenet.empleados.repo.EmpleadoRepository;
import prueba.cidenet.empleados.utils.EmpleadosUtils;


@Service
public class CrudServices {

    private String dominioCol="@cidenet.com.co";
    private String dominioUsa="@cidenet.com.us";


    @Autowired
    private EmpleadoRepository empleadoRepository;


    public List <Empleado> consultarEmppleado(){
        return empleadoRepository.findAll();

    }

    public Empleado crearEmpleado(Empleado empleado) throws Exception{

        String mensaje = this.validarEmpleado(empleado);

       /* Optional<Empleado> e= empleadoRepository.findById(empleado.getId());
        if(e.isPresent()){
            throw new Exception("El id ya se encuentra registrado");
        }*/

        empleado.setEmail(this.crearCorreo(empleado));

        return empleadoRepository.save(empleado);

    }

    public Empleado editarEmpleado(String id,Empleado empleado) throws Exception{

        Empleado empleadoFromDb = empleadoRepository.findById(id).orElseThrow(RuntimeException::new);
        
        String mensaje = this.validarEmpleado(empleado);

        empleadoFromDb.setPrimerNombre(empleado.getPrimerNombre());
        empleadoFromDb.setPrimerApellido(empleado.getPrimerApellido());

         return empleadoRepository.save(empleadoFromDb); 

    }

    public void eliminarEmpleado(String id){

        Empleado empleado = empleadoRepository.findById(id).orElseThrow(RuntimeException::new);

        empleadoRepository.delete(empleado);
    }


    public String validarEmpleado(Empleado empleado) throws Exception{

        String mensaje = "";
        mensaje+=EmpleadosUtils.contarCaracteres(empleado.getId());
        mensaje+=EmpleadosUtils.contarCaracteres(empleado.getPrimerNombre());
        mensaje+=EmpleadosUtils.contarCaracteres(empleado.getPrimerApellido());
        mensaje+=EmpleadosUtils.contarCaracteres(empleado.getSegundoApellido());
        mensaje+=EmpleadosUtils.contarCaracteres(empleado.getPais());
        mensaje+=EmpleadosUtils.contarCaracteres(empleado.getArea());

        if(empleado.getOtrosNombres().length()>50){
            mensaje+="Otros nombres No puede tener una longitud mayor a 50";
        }

        if(!mensaje.equals("")){
            throw new Exception(mensaje);
        }
        mensaje+=EmpleadosUtils.validacionCaracteres(empleado.getPrimerNombre());
        mensaje+=EmpleadosUtils.validacionCaracteres(empleado.getOtrosNombres());
        mensaje+=EmpleadosUtils.validacionCaracteres(empleado.getPrimerApellido());
        mensaje+=EmpleadosUtils.validacionCaracteres(empleado.getSegundoApellido());
        return mensaje;
        
    }

    public String crearCorreo(Empleado empleado){

        String emailNuevo = empleado.getPrimerNombre()+"."+ empleado.getPrimerApellido();
        
        if(empleado.getPais().equals("Colombia")){
            emailNuevo=emailNuevo+dominioCol;

        }else{
            emailNuevo=emailNuevo+dominioUsa;
        }
        return emailNuevo.trim();
    }
    
}
