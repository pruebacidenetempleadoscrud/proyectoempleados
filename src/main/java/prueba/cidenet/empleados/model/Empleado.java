package prueba.cidenet.empleados.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;


@Data
@Document
public class Empleado {


    @Id
    private String id;
    private String tipoId;
    private String primerNombre;
    private String otrosNombres;
    private String primerApellido;
    private String segundoApellido;
    private String pais;
    private String email;
    private String fechaIngreso;
    private String fechaRegistro;
    private String area;
    private String estado;

}
